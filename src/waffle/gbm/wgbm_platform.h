// Copyright 2012 Intel Corporation
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <stdbool.h>
#include <stdlib.h>

#undef linux

#include "wegl_platform.h"
#include "wcore_util.h"
#include "linux_gbm.h"

struct linux_platform;

struct wgbm_platform {
    struct wegl_platform wegl;
    struct linux_platform *linux;
    struct linux_gbm *gbm;
};

DEFINE_CONTAINER_CAST_FUNC(wgbm_platform,
                           struct wgbm_platform,
                           struct wegl_platform,
                           wegl)

bool
wgbm_platform_init(struct wgbm_platform *self);

bool
wgbm_platform_teardown(struct wgbm_platform *self);

struct wcore_platform*
wgbm_platform_create(void);

bool
wgbm_platform_destroy(struct wcore_platform *wc_self);

bool
wgbm_dl_can_open(struct wcore_platform *wc_self,
                 int32_t waffle_dl);

void*
wgbm_dl_sym(struct wcore_platform *wc_self,
            int32_t waffle_dl,
            const char *name);

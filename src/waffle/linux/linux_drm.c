#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "wcore_error.h"
#include "wcore_util.h"
#include "linux_drm.h"

bool
linux_drm_destroy(struct linux_drm *self)
{
    if (self->fd >= 0)
        drmClose(self->fd);
    free(self);
}

static int
linux_drm_find_plane_prop(struct linux_drm *self, const char *name)
{
    uint32_t p;
    uint32_t plane;
    drmModeObjectPropertiesPtr plane_props = NULL;
    drmModePropertyPtr prop = NULL;
    int ret = 0;

    for (plane = 0; plane < self->plane_resources->count_planes; plane++) {
        uint32_t pid = self->plane_resources->planes[plane];
        plane_props = drmModeObjectGetProperties(self->fd, pid, DRM_MODE_OBJECT_PLANE);
        if (!plane_props) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "Could not query properties for plane %d %m.", pid);
            ret = -ENOENT;
            goto done;
        }

        for (p = 0; p < plane_props->count_props; p++) {
            drmModePropertyPtr prop;
            if (prop) {
                if (strcmp("name", prop->name) == 0) {
                    ret = 1;
                    goto done;
                }
            }
            drmModeFreeProperty(prop);
            prop = NULL;
        }

        drmModeFreeObjectProperties(plane_props);
        plane_props = NULL;
    }
done:
    if (prop)
        drmModeFreeProperty(prop);
    if (plane_props)
        drmModeFreeObjectProperties(plane_props);
    return ret;
}

struct linux_drm *
linux_drm_create(int dev_minor)
{
    int ret;
    uint64_t atomic = 0;
    char dev_name[sizeof(DRM_DEV_NAME) + sizeof(DRM_DIR_NAME) + 16];
    struct linux_drm *self = wcore_calloc(sizeof(*self));

    self->fd = -1;

    snprintf(dev_name, sizeof(dev_name), DRM_DEV_NAME, DRM_DIR_NAME, dev_minor);

    self->fd = open(dev_name, O_RDWR, 0);

    if (self->fd < 0) {
        wcore_errorf(WAFFLE_ERROR_FATAL,
                     "open(\"%s\") failed: %m",
                     dev_name);
        goto error;
    }

    ret = drmSetMaster(self->fd);
    if (ret != 0) {
        wcore_errorf(WAFFLE_ERROR_FATAL,
                     "Setting drmMaster on %s failed: %m",
                     dev_name);
        goto error;
    }

    /* Set universal planes cap if possible. Ignore any errors. */
    drmSetClientCap(self->fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1);

    ret = drmGetCap(self->fd, DRM_CLIENT_CAP_ATOMIC, &atomic);
    if (!ret && atomic) {
        self->atomic = true;
        ret = drmSetClientCap(self->fd, DRM_CLIENT_CAP_ATOMIC, 1);
        if (ret < 0) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL,
                         "Setting atomic cap on %s failed: %m",
                         dev_name);
                         self->atomic = false;
        }
    }

    self->resources = drmModeGetResources(self->fd);
    if (!self->resources) {
            wcore_errorf(WAFFLE_ERROR_FATAL,
                         "Could not get drm resources %s",
                         dev_name);
    }

    self->plane_resources = drmModeGetPlaneResources(self->fd);
    if (!self->plane_resources) {
            wcore_errorf(WAFFLE_ERROR_FATAL,
                         "Could not get drm plane resources %s",
                         dev_name);
    }

    self->in_fence = (linux_drm_find_plane_prop(self, "IN_FENCE") == 1);
    self->out_fence = (linux_drm_find_plane_prop(self, "OUT_FENCE_PTR") == 1);

    return self;

error:
    linux_drm_destroy(self);
    return NULL;
}

int
linux_drm_scan(bool display, const char *driver, bool atomic, bool in_fence, bool out_fence)
{
    int d;
    for (d = 0; d < DRM_MAX_MINOR; d++) {
        struct linux_drm *ld;

        ld = linux_drm_create(d);
        if (!ld) {
            continue;
        }

        if (atomic && !ld->atomic) {
            linux_drm_destroy(ld);
            continue;
        }

        if (in_fence && !ld->in_fence) {
            linux_drm_destroy(ld);
            continue;
        }

        if (out_fence && !ld->out_fence) {
            linux_drm_destroy(ld);
            continue;
        }

        if (display) {
            if (ld->resources->count_connectors == 0) {
                linux_drm_destroy(ld);
                continue;
            }
            /* now lets see if there is anything actually attached */
        }

        if (driver) {
            drmVersionPtr version;
            version = drmGetVersion(ld->fd);
            if (version) {
                if (strcmp(driver, version->name) != 0) {
                    drmFreeVersion(version);
                    linux_drm_destroy(ld);
                    continue;
                }
                drmFreeVersion(version);
            }
        }

        linux_drm_destroy(ld);
        return d;
    }
    return -1;
}

static int32_t
atomic_set_prop(struct linux_drm *self,
                drmModeAtomicReqPtr pset,
                uint32_t id,
                drmModeObjectPropertiesPtr props,
                const char *name,
                uint64_t value)
{
    uint32_t u;
    int32_t ret;
    drmModePropertyPtr prop;

    for (u = 0; u < props->count_props; u++) {
        prop = drmModeGetProperty(self->fd, props->props[u]);
        if (!prop)
            continue;
        if (strcmp(prop->name, name)) {
            drmModeFreeProperty(prop);
            continue;
        }
        ret = drmModeAtomicAddProperty(pset, id, prop->prop_id, value);
        if (ret < 0)
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "setting atomic property %s failed with %d\n", name, ret);
        else
            ret = 0;
        drmModeFreeProperty(prop);
        return ret;
    }
    wcore_errorf(WAFFLE_ERROR_INTERNAL, "could not find atomic property %s\n", name);
    return -ENOENT;
}

static int
linux_drm_find_best_monitor(struct linux_drm *self, uint32_t *connector_id)
{
}

static int
linux_drm_find_connector_path(struct linux_drm *self, uint32_t connector_id, uint32_t *crtc_id)
{
}

static int
linux_drm_is_primary_plane(struct linux_drm *self, uint32_t plane_id)
{
    uint32_t p;
    bool found = false;
    int ret = -1;

    drmModeObjectPropertiesPtr props;
    props = drmModeObjectGetProperties(self->fd,
                                       plane_id,
                                       DRM_MODE_OBJECT_PLANE);
    if (!props) {
        wcore_errorf(WAFFLE_ERROR_INTERNAL, "Unable to get plane properties: %m");
        return -1;
    }

    for (p = 0; p < props->count_props && !found; p++) {
        drmModePropertyPtr prop;
        prop = drmModeGetProperty(self->fd, props->props[p]);
        if (prop) {
            if (strcmp("type", prop->name) == 0) {
                    found = true;
                    ret = (props->prop_values[p] == DRM_PLANE_TYPE_PRIMARY);
            }
            drmModeFreeProperty(prop);
        }
    }

    drmModeFreeObjectProperties(props);

    return ret;
}

static uint32_t
linux_drm_get_plane_crtc(struct linux_drm *self, uint32_t plane_id)
{
    uint32_t p;
    bool found = false;
    uint32_t ret = 0;

    drmModeObjectPropertiesPtr props;
    props = drmModeObjectGetProperties(self->fd,
                                       plane_id,
                                       DRM_MODE_OBJECT_PLANE);
    if (!props) {
        wcore_errorf(WAFFLE_ERROR_INTERNAL, "Unable to get plane properties: %m");
        return -1;
    }

    for (p = 0; p < props->count_props && !found; p++) {
        drmModePropertyPtr prop;
        prop = drmModeGetProperty(self->fd, props->props[p]);
        if (prop) {
            if (strcmp("CRTC_ID", prop->name) == 0) {
                    found = true;
                    ret = (uint32_t)props->prop_values[p];
            }
            drmModeFreeProperty(prop);
        }
    }

    drmModeFreeObjectProperties(props);

    return ret;
}

static bool
is_crtc_possible(struct linux_drm *self,
                 uint32_t crtc_id,
                 uint32_t mask)
{
    int32_t crtc;
    for (crtc = 0; crtc < self->resources->count_crtcs; crtc++)
        if (self->resources->crtcs[crtc] == crtc_id)
            return !!(mask & (1u << crtc));

    return false;
}

#define CHECK(fn) do { ret = fn; if (ret < 0) goto error; } while (0)
static int
linux_drm_atomic_mode_set(struct linux_drm *self, uint32_t fb_id, uint32_t crtc_id, uint32_t connector_id, const drmModeModeInfoPtr mode_info)
{
    int ret;
    drmModeAtomicReqPtr pset = NULL;
    int32_t crtc, conn;
    uint32_t plane;
    uint32_t mode_id = 0;
    drmModeObjectPropertiesPtr crtc_props = NULL;
    drmModeObjectPropertiesPtr plane_props = NULL;
    drmModeObjectPropertiesPtr conn_props = NULL;

    pset = drmModeAtomicAlloc();
    if (!pset) {
        ret = -ENOMEM;
        wcore_errorf(WAFFLE_ERROR_INTERNAL, "Atomic alloc failed: %m");
        goto error;
    }


    for (crtc = 0; crtc < self->resources->count_crtcs; crtc++) {
        uint32_t cid = self->resources->crtcs[crtc];

        crtc_props = drmModeObjectGetProperties(self->fd, cid, DRM_MODE_OBJECT_CRTC);

        if (!crtc_props) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "Could not query properties for crtc %d %m.", cid);
            if (cid != crtc_id)
                continue;
            ret = -ENOENT;
            goto error;
        }

        if (cid == crtc_id) {
            CHECK(drmModeCreatePropertyBlob(self->fd,
                                            mode_info,
                                            sizeof(*mode_info),
                                            &mode_id));
            /* self->crtc->mode has been set during init */
            CHECK(atomic_set_prop(self, pset, cid, crtc_props, "MODE_ID", mode_id));
            CHECK(atomic_set_prop(self, pset, cid, crtc_props, "ACTIVE", 1));
            /* Reset color matrix to identity and gamma/degamma LUTs to pass through,
             * ignore errors in case they are not supported. */
            atomic_set_prop(self, pset, cid, crtc_props, "CTM", 0);
            atomic_set_prop(self, pset, cid, crtc_props, "DEGAMMA_LUT", 0);
            atomic_set_prop(self, pset, cid, crtc_props, "GAMMA_LUT", 0);
        } else {
            CHECK(atomic_set_prop(self, pset, cid, crtc_props, "MODE_ID", 0));
            CHECK(atomic_set_prop(self, pset, cid, crtc_props, "ACTIVE", 0));
        }

        drmModeFreeObjectProperties(crtc_props);
        crtc_props = NULL;
    }

    for (plane = 0; plane < self->plane_resources->count_planes; plane++) {
        drmModePlanePtr planeobj;
        uint32_t pid = self->plane_resources->planes[plane];
        uint32_t possible_crtcs;
        int primary;

        planeobj = drmModeGetPlane(self->fd, pid);
        if (!planeobj) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "Could not query plane object for plane %d %m.", pid);
            ret = -ENOENT;
            goto error;
        }

        possible_crtcs = planeobj->possible_crtcs;
        drmModeFreePlane(planeobj);

        primary = linux_drm_is_primary_plane(self, pid);

        plane_props = drmModeObjectGetProperties(self->fd, pid, DRM_MODE_OBJECT_PLANE);
        if (!plane_props) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "Could not query properties for plane %d %m.", pid);
            ret = -ENOENT;
            goto error;
        }

        if (is_crtc_possible(self, crtc_id, possible_crtcs) && primary == 1) {
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "FB_ID", fb_id));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "CRTC_ID", crtc_id));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "CRTC_X", 0));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "CRTC_Y", 0));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "CRTC_W", mode_info->hdisplay));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "CRTC_H", mode_info->vdisplay));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "SRC_X", 0));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "SRC_Y", 0));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "SRC_W", mode_info->hdisplay << 16));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "SRC_H", mode_info->vdisplay << 16));
        } else {
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "FB_ID", 0));
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "CRTC_ID", 0));
        }

        drmModeFreeObjectProperties(plane_props);
        plane_props = NULL;
    }

    for (conn = 0; conn < self->resources->count_connectors; conn++) {
        uint32_t cid = self->resources->connectors[conn];

        conn_props = drmModeObjectGetProperties(self->fd, cid, DRM_MODE_OBJECT_CONNECTOR);
        if (!conn_props) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "Could not query properties for connector %d %m.", cid);
            if (cid != connector_id)
                continue;
            ret = -ENOENT;
            goto error;
        }
        if (cid == connector_id)
            CHECK(atomic_set_prop(self, pset, cid, conn_props, "CRTC_ID", crtc_id));
        else
            CHECK(atomic_set_prop(self, pset, cid, conn_props, "CRTC_ID", 0));
        drmModeFreeObjectProperties(conn_props);
        conn_props = NULL;
    }

    ret = drmModeAtomicCommit(self->fd,
                              pset,
                              DRM_MODE_ATOMIC_ALLOW_MODESET,
                              NULL);
    if (ret < 0) {
    } else {
        ret = 0;
    }

error:
    if (mode_id)
        drmModeDestroyPropertyBlob(self->fd, mode_id);

    if (crtc_props)
        drmModeFreeObjectProperties(crtc_props);

    if (conn_props)
        drmModeFreeObjectProperties(conn_props);

    if (plane_props)
        drmModeFreeObjectProperties(plane_props);

    if (pset)
        drmModeAtomicFree(pset);
    return ret;
}

static int
linux_drm_atomic_flip(struct linux_drm *self, uint32_t crtc_id, uint32_t fb_id, int in_fence, int *out_fence, bool async)
{
    int ret;
    uint32_t plane;
    drmModeObjectPropertiesPtr plane_props = NULL;
    drmModeAtomicReqPtr pset = NULL;

    pset = drmModeAtomicAlloc();
    if (!pset) {
        ret = -ENOMEM;
        wcore_errorf(WAFFLE_ERROR_INTERNAL, "Atomic alloc failed: %m");
        goto error;
    }

    for (plane = 0; plane < self->plane_resources->count_planes; plane++) {
        uint32_t pid = self->plane_resources->planes[plane];
        uint32_t cid;
        int primary;

        cid = linux_drm_get_plane_crtc(self, pid);
        if (cid != crtc_id)
            continue;

        primary = linux_drm_is_primary_plane(self, pid);
        if (primary != 1)
            continue;

        plane_props = drmModeObjectGetProperties(self->fd, pid, DRM_MODE_OBJECT_PLANE);
        if (!plane_props) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL, "Could not query properties for plane %d %m.", pid);
            ret = -ENOENT;
            goto error;
        }

        CHECK(atomic_set_prop(self, pset, pid, plane_props, "FB_ID", fb_id));
        if (in_fence)
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "IN_FENCE", crtc_id));
        if (out_fence)
            CHECK(atomic_set_prop(self, pset, pid, plane_props, "OUT_FENCE_PTR", (uintptr_t)out_fence));

        drmModeFreeObjectProperties(plane_props);
        plane_props = NULL;
    }

    ret = drmModeAtomicCommit(self->fd,
                              pset,
                              async ? DRM_MODE_PAGE_FLIP_ASYNC : 0,
                              NULL);
    if (ret < 0) {
    } else {
        ret = 0;
    }

error:
    if (plane_props)
        drmModeFreeObjectProperties(plane_props);

    if (pset)
        drmModeAtomicFree(pset);
    return ret;
}
#undef CHECK

int
linux_drm_flip(struct linux_drm *self, uint32_t crtc_id, uint32_t fb_id, int in_fence, int *out_fence, bool async)
{
    int ret;

    if (self->atomic) {
        ret = linux_drm_atomic_flip(self, crtc_id, fb_id, in_fence, out_fence, async);
    } else {
        ret = drmModePageFlip(self->fd, crtc_id, fb_id, DRM_MODE_PAGE_FLIP_EVENT | (async ? DRM_MODE_PAGE_FLIP_ASYNC : 0), NULL);
    }

    return ret;
}

int
linux_drm_mode_set(struct linux_drm *self, uint32_t fb_id, uint32_t crtc_id, uint32_t connector_id, const drmModeModeInfoPtr mode_info)
{
    int ret;

    if (self->atomic) {
        ret = linux_drm_atomic_mode_set(self, fb_id, crtc_id, connector_id, mode_info);
    } else {
        ret = drmModeSetCrtc(self->fd,
                             crtc_id,
                             fb_id,
                             0, 0,  // x,y
                             &connector_id,
                             1,  // connector_count
                             mode_info); // mode
    }
    return ret;
}


#include <dlfcn.h>

#include "wcore_error.h"
#include "wcore_util.h"
#include "linux_gbm.h"
#include "linux_dl.h"


static const char *libgbm_filename = "libgbm.so.1";


bool
linux_gbm_destroy(struct linux_gbm *self)
{
    int error = 0;
    if (self->gbmHandle) {
        error = dlclose(self->gbmHandle);
        if (error) {
            wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                         "dlclose(\"%s\") failed: %s",
                         libgbm_filename, dlerror());
            return false;
        }
    }
    free(self);
    return true;
}

struct linux_gbm *
linux_gbm_create(void)
{
    struct linux_gbm *self = wcore_calloc(sizeof(*self));

    self->gbmHandle = dlopen(libgbm_filename, RTLD_LAZY | RTLD_LOCAL);
    if (!self->gbmHandle) {
        wcore_errorf(WAFFLE_ERROR_FATAL,
                     "dlopen(\"%s\") failed: %s",
                     libgbm_filename, dlerror());
        goto error;
    }

#define RETRIEVE_GBM_SYMBOL(type, function, required, args)            \
    self->function = dlsym(self->gbmHandle, #function);                \
    if (required && !self->function) {                                 \
        wcore_errorf(WAFFLE_ERROR_FATAL,                             \
                     "dlsym(\"%s\", \"" #function "\") failed: %s",    \
                     libgbm_filename, dlerror());                      \
        goto error;                                                    \
    }

    GBM_FUNCTIONS(RETRIEVE_GBM_SYMBOL);
#undef RETRIEVE_GBM_SYMBOL


    return self;
error:
    linux_gbm_destroy(self);
    return NULL;
}



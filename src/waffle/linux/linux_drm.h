#pragma once


#include <xf86drm.h>
#include <xf86drmMode.h>


struct linux_drm {
    int fd;

    bool atomic;
    bool in_fence;
    bool out_fence;

    drmModeRes* resources;
    drmModePlaneResPtr plane_resources;
};

struct linux_drm *
linux_drm_create(int dev_minor);

bool
linux_drm_destroy(struct linux_drm *self);

int
linux_drm_scan(bool display, const char *driver, bool atomic, bool in_fence, bool out_fence);

int
linux_drm_mode_set(struct linux_drm *self, uint32_t fb_id, uint32_t crtc_id, uint32_t connector_id, const drmModeModeInfoPtr mode_info);

int
linux_drm_flip(struct linux_drm *self, uint32_t crtc_id, uint32_t fb_id, int in_fence, int *out_fence, bool async);



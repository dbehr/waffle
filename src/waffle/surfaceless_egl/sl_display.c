// Copyright 2016 Google
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "wcore_error.h"

#include "sl_display.h"
#include "sl_platform.h"

#include <stdio.h>

bool
sl_display_destroy(struct wcore_display *wc_self)
{
    struct sl_display *self = sl_display(wegl_display(wc_self));
    bool ok = true;

    if (!self)
        return ok;

    ok &= wegl_display_teardown(&self->wegl);
    free(self);
    return ok;
}

static EGLDeviceEXT
find_device_from_minor(struct wegl_platform *plat)
{
    struct sl_platform *sl_plat = sl_platform(plat);
    EGLDeviceEXT devices[64]; // max DRM devices
    EGLint num_devices = 0;
    EGLint d;

    if (!plat->EXT_platform_device
        || !plat->EXT_device_enumeration
        || !plat->EXT_device_query) {
        wcore_errorf(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM,
                     "required extensions for device platform are not available %s",
                     plat->client_extensions);
        return NULL;
    }

    if (!plat->eglQueryDevicesEXT || !plat->eglQueryDeviceStringEXT) {
        return NULL;
    }

    if (!plat->eglQueryDevicesEXT(64, devices, &num_devices))
        return NULL;

    for (d = 0; d < num_devices; d++) {
        int dminor = -1;
        const char *devpath = plat->eglQueryDeviceStringEXT(devices[d],
                                                            EGL_DRM_DEVICE_FILE_EXT);
        char *mstr;

        if (!devpath)
            continue;

        if (sl_plat->render_minor < 0)
            return devices[d];

        mstr = strstr(devpath, "card");
        if (mstr) {
            dminor = strtol(mstr + 4, &mstr, 10);
        } else {
            mstr = strstr(devpath, "renderD");
            if (mstr) {
                dminor = strtol(mstr + 7, &mstr, 10);
            }
        }

        if (dminor < 0)
            continue;

        if (dminor == sl_plat->render_minor)
            return devices[d];
    }

    return NULL;
}



struct wcore_display*
sl_display_connect(struct wcore_platform *wc_plat, const char *name)
{
    struct sl_display *self;
    bool ok = true;

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    if (name != NULL) {
        wcore_errorf(WAFFLE_ERROR_BAD_PARAMETER,
                     "parameter 'name' is not NULL");
        goto fail;
    }

    if (wc_plat->waffle_platform == WAFFLE_PLATFORM_DEVICE_EGL) {
        EGLDeviceEXT device = find_device_from_minor(wegl_platform(wc_plat));
        if (device == NULL) {
            wcore_errorf(WAFFLE_ERROR_INTERNAL,
                         "could not find render device");
            goto fail;
        }
        ok = wegl_display_init(&self->wegl, wc_plat, device);
    } else {
        ok = wegl_display_init(&self->wegl, wc_plat, NULL);
    }

    if (!ok)
        goto fail;

    return &self->wegl.wcore;

fail:
    sl_display_destroy(&self->wegl.wcore);
    return NULL;
}
